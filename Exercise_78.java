//78. Write a Java program to test that a given array of integers of length 2 contains a 4 or a 7.

public class Exercise_78 {
    public static void main(String[] args) {
        int[] array = {5, 7};
        System.out.println((array[0] == 4 || array[0] == 7 || array[1] == 4 || array[1] == 7));
    }
}
