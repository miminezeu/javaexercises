//6. Write a Java program to print the sum (addition), multiply, subtract, divide and remainder of two numbers.
import java.util.Scanner;

public class PrintOperators_6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input first number: ");
        int firstNum = scanner.nextInt();
        System.out.println("Input second number: ");
        int secondNum = scanner.nextInt();

        int Add = firstNum + secondNum;
        int Sub = firstNum - secondNum;
        int Mul = firstNum * secondNum;
        int Div = firstNum / secondNum;
        int Mod = firstNum % secondNum;

        System.out.println("\n" + firstNum + " + " + secondNum + " = " + Add);
        System.out.println(firstNum + " - " + secondNum + " = " + Sub);
        System.out.println(firstNum + " x " + secondNum + " = " + Mul);
        System.out.println(firstNum + " / " + secondNum + " = " + Div);
        System.out.println(firstNum + " mod " + secondNum + " = " + Mod);

    }
}
