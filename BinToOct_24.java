//24. Write a Java program to convert a binary number to a Octal number.

import java.util.Scanner;

public class BinToOct_24 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a Binary Number: ");
        int Oct = Integer.parseInt(scanner.nextLine(), 2);

        System.out.printf("Octal number: " + Integer.toOctalString(Oct));
    }
}
