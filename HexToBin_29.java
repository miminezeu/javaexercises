//29. Write a Java program to convert a hexadecimal to a binary number.

import java.util.Scanner;

public class HexToBin_29 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter Hexadecimal Number : ");
        int hex = Integer.parseInt(scanner.next(), 16);

        System.out.println("Equivalent Binary Number is: " + Integer.toBinaryString(hex));

    }
}
