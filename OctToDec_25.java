//25. Write a Java program to convert a octal number to a decimal number

import java.util.Scanner;

public class OctToDec_25 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input any octal number: ");
        int oct = Integer.parseInt(scanner.nextLine(), 8);

        System.out.println("Equivalent decimal number: " + Integer.toString(oct, 10));
    }
}
