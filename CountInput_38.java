//38. Write a Java program to count the letters, spaces, numbers and other characters of an input string.

import java.util.Scanner;

public class CountInput_38 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("THe string is: ");
        char[] charCount = scanner.nextLine().toCharArray();

        int letterCount = 0;
        int spaceCount = 0;
        int numberCount = 0;
        int otherCount = 0;

        for (int i = 0; i <= charCount.length - 1; i++) {
            boolean isWord = false;

            if (Character.isLetter(charCount[i])) {
                    letterCount++;
                } else if (Character.isSpaceChar(charCount[i])) {
                    spaceCount++;
                } else if (Character.isDigit(charCount[i])){
                    numberCount++;
                } else {
                    otherCount++;
            }
        }
        System.out.printf("\nletter: %d", letterCount);
        System.out.printf("\nspace: %d", spaceCount);
        System.out.printf("\nnumber: %d", numberCount);
        System.out.printf("\nother: %d", otherCount);
    }
}

