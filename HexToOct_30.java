//30. Write a Java program to convert a hexadecimal to a octal number.

import java.util.Scanner;

public class HexToOct_30 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a hexadecimal number: ");
        int hex = Integer.parseInt(scanner.nextLine(), 16);

        System.out.println("Equivalent of octal number is: " + Integer.toOctalString(hex));
    }
}
