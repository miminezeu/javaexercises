//77. Write a Java program to create a new array of length 2 from two arrays of
// integers with three elements and the new array will contain the first and last
// elements from the two arrays.
//Test Data:
//array1 = 50, -20, 0
//array2 = 5, -50, 10

import java.util.Arrays;

public class Exercise_77 {
    public static void main(String[] args) {
        int[] array1 = {50, -20, 0};
        int[] array2 = {5, -50, 10};

        int[] array3 = {array1[0], array2[array2.length-1]};
        System.out.println("Array1 : " + Arrays.toString(array1));
        System.out.println("Array2 : " + Arrays.toString(array2));
        System.out.println("New Array: " + Arrays.toString(array3));
    }
}
