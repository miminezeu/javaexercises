//92. Write a Java program to count the number of even and odd elements in a given array of integers.

import java.util.Arrays;

public class Exercise_92 {
    public static void main(String[] args) {
        int[] array = {5, 7, 2, 4, 9};
        int even = 0;
        int odd = 0;

        System.out.println("Original Array: " + Arrays.toString(array));
        for (int i = 0; i <= 4; i++){
            if (array[i] % 2 == 0){
                even++;
            } else
                odd++;

        }
        System.out.printf("\nNumber of even elements in the array: %d", even);
        System.out.printf("\nNumber of odd elements in the array: %d", odd);

    }
}
