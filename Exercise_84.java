/*84. Write a Java program to take the last three characters from a given string and add the three characters at both the front and back of the string. String length must be greater than three and more. Go to the editor
Test data: "Python" will be "honPythonhon"*/

public class Exercise_84 {
    public static void main(String[] args) {
        String python = "Python";
        System.out.println(python.substring(3, 6) + python + python.substring(3, 6));
    }
}
