//79. Write a Java program to rotate an array (length 3) of integers in left direction.

import java.util.Arrays;

public class Exercise_79 {
    public static void main(String[] args) {
        int[] array = {20, 30, 40};

        System.out.println("Original Array: " + Arrays.toString(array));
        int[] rotated = {array[1], array[array.length-1], array[0]};
        System.out.println("Rotated Array: " + Arrays.toString(rotated));
    }
}
