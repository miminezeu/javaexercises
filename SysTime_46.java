//46. Write a Java program to display the system time.
import java.util.Date;

public class SysTime_46 {
    public static void main(String[] args) {
        Date today = new Date();

        System.out.print("Current Date time: " + today);
    }
}
