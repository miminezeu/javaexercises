//32. Write a Java program to compare two numbers.

import java.util.Scanner;

public class CompareNumbers_32 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.printf("Input first integer: ");
        int firstNum = scanner.nextInt();

        System.out.printf("Input second integer: ");
        int secondNum = scanner.nextInt();

        if (firstNum > secondNum) {
            System.out.printf("%d > %d\n", firstNum, secondNum);
        }
        if (firstNum < secondNum) {
            System.out.printf("%d < %d\n", firstNum, secondNum);
        }
        if (firstNum == secondNum) {
            System.out.printf("%d == %d\n", firstNum, secondNum);
        }
        if (firstNum <= secondNum) {
            System.out.printf("%d <= %d\n", firstNum, secondNum);
        }
        if (firstNum >= secondNum) {
            System.out.printf("%d >= %d\n", firstNum, secondNum);
        }
        if (firstNum != secondNum) {
            System.out.printf("%d != %d\n", firstNum, secondNum);
        }
    }
}
