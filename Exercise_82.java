//82. Write a Java program to find the largest element between first, last, and middle values from an array of integers (even length).

import java.util.Arrays;

public class Exercise_82 {
    public static void main(String[] args) {
        int[] array = {20, 30, 40, 50, 67};
        int largest = 0;
        System.out.println("Original Array: " + Arrays.toString(array));

        if (array[0] > array[2] && (array[0] > array[array.length-1])){
            largest = array[0];
        } else if (array[array.length-1] > array[2]) {
            largest = array[array.length-1];
        } else if (array[2] > array[array.length-1]) {
            largest = array[2];
        }

        /*int max_val = array[0];
        if(max_val <= array[array.length-1])
            max_val = array[array.length-1];
        if(max_val <= array[array.length/2])
            max_val = array[array.length/2];*/

        System.out.println("Largest element between first, last and middle values: " + largest);
    }
}
