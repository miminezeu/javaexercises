//5. Write a Java program that takes two numbers as input and display the product of two numbers.

import java.util.Scanner;

public class DisplayInput_5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input first number: ");
        int firstNum = scanner.nextInt();
        System.out.println("Input second number: ");
        int secondNum = scanner.nextInt();

        int Product = firstNum * secondNum;

        System.out.println("\n" + firstNum + " x " + secondNum + " = " + Product);
    }
}
