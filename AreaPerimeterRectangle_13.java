//13. Write a Java program to print the area and perimeter of a rectangle.
// Test Data: Width = 5.5 Height = 8.5

import java.text.DecimalFormat;

public class AreaPerimeterRectangle_13 {
    public static void main(String[] args) {
        DecimalFormat decFormat = new DecimalFormat("0.00");
        double width = 5.6;
        double height = 8.5;

        double area = height * width;
        double perimeter = (height + width) * 2;
        System.out.println("Area is = " + width + " * " + height + " = " + decFormat.format(area));
        System.out.println("Perimeter is = 2 * (" + width + " + " + height + ") = " + decFormat.format(perimeter));

        //no need for decimal formatting and concatenation
        //System.out.printf("Area is %.1f * %.1f = %.2f \n", width, height, area);
        //System.out.printf("Perimeter is 2 * (%.1f + %.1f) = %.2f \n", width, height, perimeter);
    }
}
