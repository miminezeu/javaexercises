//22. Write a Java program to convert a binary number to decimal number.

import java.util.Scanner;

public class BinToDec_22 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a binary Number: ");
        int Oct = Integer.parseInt(scanner.nextLine(), 2);

        System.out.printf("Decimal Number: " + Integer.toString(Oct, 10));
    }

}
