//23. Write a Java program to convert a binary number to hexadecimal number.

import java.util.Scanner;

public class BinToHex_23 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a Binary Number: ");
        int Bin = Integer.parseInt(scanner.nextLine(), 2);

        System.out.printf("HexaDecimal value: " + Integer.toHexString(Bin).toUpperCase());
    }
}
