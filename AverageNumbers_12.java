import java.util.Scanner;

public class AverageNumbers_12 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input first number: ");
        int firstNum = scanner.nextInt();

        System.out.println("Input second number: ");
        int secondNum = scanner.nextInt();

        System.out.println("Input third number: ");
        int thirdNum = scanner.nextInt();

        int average = (firstNum + secondNum + thirdNum) / 3;
        System.out.println(average);
    }
}
