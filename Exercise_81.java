//81. Write a Java program to swap the first and last elements of an array (length must be at least 1) and create a new array.

import java.util.Arrays;

public class Exercise_81 {
    public static void main(String[] args) {
        int[] array = {20, 30, 40};
        System.out.println("Original Array: " + Arrays.toString(array));
        int swap = array[0];
        array[0] = array[array.length-1];
        array[array.length-1] = swap;

        System.out.println("New array after swapping the first and last elements: " + Arrays.toString(array));
    }
}
