//40. Write a Java program to list the available character sets in charset objects.

import com.sun.org.apache.xpath.internal.res.XPATHErrorResources_sv;

import java.nio.charset.Charset;
import java.util.Set;

public class CharObjects_40 {
    public static void main(String[] args) {
        System.out.println("List of available character sets: ");
        for (String strSet : Charset.availableCharsets().keySet()){
            System.out.println(strSet);
        };
    }
}
