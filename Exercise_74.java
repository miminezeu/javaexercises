//74. Write a Java program to test if 10 appears as either the first or last element of an array of integers.
//Test Data: array = 10, -20, 0, 30, 40, 60, 10

public class Exercise_74 {
    public static void main(String[] args) {
        int[] array = {10, -20, 0, 30, 40, 60, 10};
        if (array[0] == 10 || array[6] == 10) {
            System.out.println(true);
        } else
            System.out.println(false);
       //System.out.println((array[0] == 10 && array[array.length-1] == 10));
    }


}
