//80. Write a Java program to get the larger value between first and last element of an array (length 3) of integers .

import java.util.Arrays;

public class Exercise_80 {
    public static void main(String[] args) {
        int[] array = {20, 30, 40};
        System.out.println("Original Array: " + Arrays.toString(array));

        if (array[2] >= array[0]) {
            int largerValue = array[2];
            System.out.println("Larger value between first and last element: " + largerValue);
        }
    }
}