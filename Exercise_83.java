//83. Write a Java program to multiply corresponding elements of two arrays of integers.

import java.util.Arrays;

public class Exercise_83 {
    public static void main(String[] args) {
        int[] array1 = {1, 3, -5, 4};
        int[] array2 = {1, 4, -5, -2};

        System.out.println("Array1: " + Arrays.toString(array1));
        System.out.println("\nArray2: " + Arrays.toString(array2));

        /*int mul1 = array1[0] * array2[0];
        int mul2 = array1[1] * array2[1];
        int mul3 = array1[2] * array2[2];
        int mul4 = array1[3] * array2[3];
        System.out.printf("\nResult: %d %d %d %d", mul1, mul2, mul3, mul4);*/

        String product = "";
        for (int i = 0; i < 4; i++){
            int mul1 = array1[i];
            int mul2 = array2[i];

            product += Integer.toString(mul1 * mul2) + " ";
        }
        System.out.println("\nResult: " + product);
    }
}
