//94. Write a Java program to rearrange all the elements of an given array of integers so that all the odd numbers come before all the even numbers.

import jdk.internal.org.objectweb.asm.commons.SerialVersionUIDAdder;

import java.lang.reflect.Array;
import java.util.Arrays;

public class Exercise_94 {
    public static void main(String[] args) {
        //int[] array = {6, 8, 5, 7, 2, 4, 9, 1, 3};
        int[] array = {1, 7, 8, 5, 7, 13, 0, 2, 4, 9};
        System.out.println("Original Array: " + Arrays.toString(array));

        String odd = "";
        String even = "";
        int i = 0;
        for (; i < array.length; i++) {
            if (array[i] % 2 != 0) {
                odd += array[i] + " ";
            } else
                even += array[i] + " ";
        }
        array[i] = Integer.parseInt(odd + even); //ERROR HERE

        System.out.println("New Array: " + Arrays.toString(array));
    }
}
