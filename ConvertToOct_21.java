//21. Write a Java program to convert a decimal number to octal number.

import java.util.Scanner;

public class ConvertToOct_21 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Input a Decimal Number: ");
        int oct = Integer.parseInt(scanner.nextLine());

        System.out.println("Binary number is: " + Integer.toOctalString(oct));
    }
}
